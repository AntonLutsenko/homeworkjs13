theme.setAttribute(
	"href",
	window.localStorage.getItem("currentTheme") || "./styles/style.css"
);

document.querySelector(".btn-theme").addEventListener("click", function () {
	let theme = document.getElementById("theme");
	if (theme.getAttribute("href") == "./styles/style.css") {
		theme.href = "./styles/light_style.css";
	} else {
		theme.href = "./styles/style.css";
	}
});

window.addEventListener("beforeunload", function () {
	this.window.localStorage.setItem("currentTheme", theme.getAttribute("href"));
});
